import './App.css';
import BarraNav from './componentes/BarraNav';
import {HolaMundo, Variable }from './componentes/HolaMundo';
import {StyledButton} from './componentes/Personalizados';
import { StyleDiv } from './componentes/Personalizados';
import { Contenedor } from './componentes/Personalizados';
import { BrowserRouter, Routes, Route } from "react-router-dom"; 
import {Reservas} from './componentes/Reservas';
import {Noticias} from './componentes/Noticias';
import React from 'react';
import Deportes from '../../ejercicio2/src/componentes/Deportes';

function App() {
  return (
  
    <>
     <BrowserRouter>{/*Navegador de ruta,*/ }
     <div>
     <Routes>
       
       <Route path='/' element={<BarraNav/>}></Route>
       <Route path='/reservas' element={<Reservas/>}></Route>
       <Route path='/noticias' element={<Noticias/>}></Route>
       <Route path='/deportes' element={<Deportes/>}></Route>

       <Route path='*' element={<h1>esta pagina no existe</h1>}> </Route>
    
    </Routes>
     </div>
     
  
   </BrowserRouter>
    </>
   
  
    )
};

export default App;
