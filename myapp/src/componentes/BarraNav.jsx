import { Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./BarraNav.css";


export default function BarraNav(){
    let buttons =(
      // la almuadilla signidfica que te lleva a la misma pagina
      //ms-4 => bootstrap es para ponerle margen al inicio
      //mt-3 => 
      //mb-2 =>
      <div>
            
            <Button href="/reservas" className="ms-4 mt-3 mb-2" variant="warning" >Reservas</Button> 
            <Button href="/noticias" className='ms-4 mt-3 mb-2' variant="info" >Noticias</Button>
            <Button href="#" className='ms-4 mt-3 mb-2' variant="warning" >Contacto</Button>
            <Button href="#" className='ms-4 mt-3 mb-2' variant="danger" >Registro</Button>
            <Button href="#" className='ms-4 mt-3 mb-2' variant="info" >Blog</Button>
        </div>

    )
    return(
        
        <header>
            <nav>
                <ul>{buttons}</ul>
            </nav>

        </header>

    );
    
}


